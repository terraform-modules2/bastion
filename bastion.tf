resource "aws_security_group" "bastion" {
  name = var.name
  vpc_id = var.vpc_id
  description = "Bastion security group (only SSH inbound access is allowed)"

  tags = {
    Name = var.name
  }
}

resource "aws_security_group_rule" "ssh_ingress" {
  type = "ingress"
  from_port = "22"
  to_port = "22"
  protocol = "tcp"
  cidr_blocks = var.allowed_cidr
  ipv6_cidr_blocks = var.allowed_ipv6_cidr
  security_group_id = aws_security_group.bastion.id

  depends_on = [aws_security_group.bastion]
}

resource "aws_security_group_rule" "ssh_sg_ingress" {
  count = length(var.allowed_security_groups)
  type = "ingress"
  from_port = "22"
  to_port = "22"
  protocol = "tcp"
  source_security_group_id = element(var.allowed_security_groups, count.index)
  security_group_id = aws_security_group.bastion.id

  depends_on = [aws_security_group.bastion]
}

resource "aws_security_group_rule" "bastion_all_egress" {
  type = "egress"
  from_port = "0"
  to_port = "65535"
  protocol = "all"

  cidr_blocks = [
    "0.0.0.0/0",
  ]

  ipv6_cidr_blocks = [
    "::/0",
  ]

  security_group_id = aws_security_group.bastion.id

  depends_on = [aws_security_group.bastion]
}

data "template_file" "user_data" {
  template = file("${path.module}/${var.user_data_file}")

  vars = {
    s3_bucket_name = var.s3_bucket_name
    s3_bucket_uri = var.s3_bucket_uri
    ssh_user = var.ssh_user
    keys_update_frequency = var.keys_update_frequency
    enable_hourly_cron_updates = var.enable_hourly_cron_updates
    additional_user_data_script = var.additional_user_data_script
  }
}

resource "aws_instance" "bastion" {
  ami = var.ami
  instance_type = var.instance_type
  subnet_id = var.subnet_id
  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]
  user_data = data.template_file.user_data.rendered
  associate_public_ip_address = var.associate_public_ip_address
  iam_instance_profile = aws_iam_instance_profile.instance-role.name

  tags = {
    Name = var.name
  }

  depends_on = [
    aws_security_group.bastion,
    aws_iam_instance_profile.instance-role,
  ]
}

resource "aws_iam_instance_profile" "instance-role" {
  name = "bastion"
  role = aws_iam_role.shiftie-ec2-bastion.name
}